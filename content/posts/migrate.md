+++
title = "サイト移行しました"
date = 2023-04-05T15:24:01+09:00
draft = false
author = "itu"
authorTwitter = "itu961r" #do not include @
cover = ""
tags = ["information"]
keywords = ["", ""]
description = "[移行先](https://nitgc-densan-club.github.io/)"
showFullContent = false
+++

今後は[こちら](https://nitgc-densan-club.github.io/)のサイトで、様々な情報を発信していく予定です


